package com.hello;

/**
 * isAlive: 表示线程是否还活着
 * Thread.currentThread()：表示当前线程
 * setName、getName：代理名称
 */
public class InfoTest {

    public static void main(String[] args) throws InterruptedException {
        System.out.println(Thread.currentThread().isAlive());

        // 代理对象+真实对象
        MyInfo info = new MyInfo("战斗机");
        Thread thread = new Thread(info);
        thread.setName("公鸡");
        thread.start();

        Thread.sleep(1000);
        System.out.println(thread.isAlive());
    }
}


class MyInfo implements Runnable {

    private String name; // 真实对象的名称

    public MyInfo(String name) {
        this.name = name;
    }

    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName() + "----->"+name);
    }
}
