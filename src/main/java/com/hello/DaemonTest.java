package com.hello;

/**
 * 守护线程：是为用户线程服务的，jvm停止不用等待守护线程执行完毕
 * 用户线程：jvm等待用户线程执行完毕才停止。默认。
 */
public class DaemonTest {

    public static void main(String[] args) {
        You you = new You();
        God god = new God();

        Thread t1 = new Thread(god);
        t1.setDaemon(true);// 将用户线程调整为守护
        t1.start();

        new Thread(you).start();
    }
}

class You implements Runnable{

    @Override
    public void run() {
        for (int i=0; i<365*100; i++) {
            System.out.println("happy life.........");
        }
    }
}

class God implements Runnable{

    @Override
    public void run() {
       while (true){
           System.out.println("god pless you......");
       }
    }
}
