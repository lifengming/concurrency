package com.hello;

/**
 * 创建线程方式二
 * 1、创建：实现Runnable，重写run方法
 * 2、启动：创建实现类对象，创建代理类对象，调用start方法
 *
 * 推荐：避免单继承的局限性，优先使用接口，方便共享资源
 */
public class StartRunnable implements Runnable {

    /**
     * 线程入口点
     */
    @Override
    public void run() {
        for(int i=0; i<5; i++){
            System.out.println("一边听歌");
        }
    }

    public static void main(String[] args) {
        // 创建实现类对象
        StartRunnable sr = new StartRunnable();
        // 创建代理类对象
        Thread t = new Thread(sr);
        t.start(); // 开启一个线程，不保证立即运行，由cpu调用

        for(int i=0; i<5; i++){
            System.out.println("一边coding");
        }
    }
}
