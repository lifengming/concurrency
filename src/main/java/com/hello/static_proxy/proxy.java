package com.hello.static_proxy;

/**
 * 静态代理模式：类是写好的。【动态代理模式：类是临时生成的】
 * 1、上层接口
 * 2、真实角色，实现上层接口
 * 3、代理角色，实现上层接口
 */
public class proxy {

    public static void main(String[] args) {
        WeddingMarry marry = new WeddingMarry(new You());
        marry.happyMarry();
    }
}
