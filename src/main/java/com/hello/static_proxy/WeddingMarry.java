package com.hello.static_proxy;

/**
 * 代理对象
 */
public class WeddingMarry implements Marry {

    // 目标对象
    private You you;

    public WeddingMarry(You you) {
        this.you = you;
    }

    @Override
    public void happyMarry() {
        ready();
        you.happyMarry();
        after();
    }

    void ready() {
        System.out.println("准备猪窝");
    }

    void after() {
        System.out.println("闹洞房");
    }
}
