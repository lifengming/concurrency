package com.hello.static_proxy;

/**
 * 真实角色
 */
public class You implements Marry {

    @Override
    public void happyMarry() {
        System.out.println("恭喜你结婚了");
    }
}
