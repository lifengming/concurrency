package com.hello.yield;

/**
 * yield礼让线程，暂停线程直接进入就绪状态
 */
public class YieldDemo01 {

    public static void main(String[] args) {
        MyYield myYield = new MyYield();
        new Thread(myYield, "aa").start();
        new Thread(myYield, "bb").start();
    }
    // result之1
    //aa-------> start
    //bb-------> start
    //aa-------> end
    //bb-------> end
}

class MyYield implements Runnable {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+ "-------> start");
        Thread.yield();;
        System.out.println(Thread.currentThread().getName()+ "-------> end");
    }
}
