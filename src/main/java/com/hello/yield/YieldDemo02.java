package com.hello.yield;

/**
 * yield礼让线程，暂停线程直接进入就绪状态
 */
public class YieldDemo02 {

    public static void main(String[] args) {
       new Thread(()->{
           for(int i=0; i<20; i++) {
               if (i%5==0) {
                   Thread.yield(); // 线程礼让
               }
               System.out.println("lambda-------" + i);
           }
       }).start();

        for(int i=0; i<20; i++) {
            System.out.println("main------"+i);
        }
    }
}
