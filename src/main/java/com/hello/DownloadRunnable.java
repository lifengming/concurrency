package com.hello;

/**
 * 多线程下载图片
 * 多张图片并发下载
 */
public class DownloadRunnable implements Runnable{

    private String url;
    private String name;

    public DownloadRunnable(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        Download d = new Download();
        d.download(url, name);
        System.out.println(name);
    }

    public static void main(String[] args) {
        DownloadRunnable dt = new DownloadRunnable("http://static2.iocoder.cn/images/Learn/08.png", "jc.jpg");
        DownloadRunnable dt2 = new DownloadRunnable("http://static2.iocoder.cn/images/Learn/07.png", "yw.jpg");
        DownloadRunnable dt3 = new DownloadRunnable("http://static2.iocoder.cn/images/Learn/06.png", "gj.jpg");

        new Thread(dt).start();
        new Thread(dt2).start();
        new Thread(dt3).start();
    }
}
