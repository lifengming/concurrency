package com.hello.state;

/**
 * sleep模拟网络延时，放大发生问题的可能性
 */
public class BlockSleep01 {

    public static void main(String[] args) {
        // 一份资源
        Web12306Runnable w = new Web12306Runnable();
        // 多个代理
        new Thread(w, "码畜").start();
        new Thread(w, "码农").start();
        new Thread(w, "蚂蟥").start();
    }
}

/**
 * 模拟抢票
 * 共享资源，并发（线程安全）
 */
class Web12306Runnable implements Runnable {

    private int ticketNums = 99;

    @Override
    public void run() {
        while (true) {
            if (ticketNums < 0) {
                break;
            }
            // 模拟网络延时,出现并发数据问题
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+ "--->" +ticketNums--);
        }
    }
}