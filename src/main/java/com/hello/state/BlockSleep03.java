package com.hello.state;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 倒计时，模拟网络阻塞
 */
public class BlockSleep03 {

    public static void main(String[] args) throws InterruptedException {
        // 倒计时10s，所以加10s
        Date endDate = new Date(System.currentTimeMillis()+1000*10);
        long end = endDate.getTime();
        while (true) {
            System.out.println(new SimpleDateFormat("mm:ss").format(endDate));
            Thread.sleep(1000);
            endDate = new Date(endDate.getTime() - 1000);
            if (end - 10000 > endDate.getTime()) {
                break;
            }
        }

    }
}
