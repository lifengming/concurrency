package com.hello.state;

/**
 * 观察线程的五个状态
 */
public class AllState {

    public static void main(String[] args) {
        Thread t = new Thread(() -> {
            for (int i=0; i<5; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("......");
            }
        });

        Thread.State state = t.getState();
        System.out.println(state); // NEW(新生状态)

        t.start();
        state = t.getState();
        System.out.println(state); // RUNNABLE(就绪+运行)

        while (state != Thread.State.TERMINATED){
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            state = t.getState();
            System.out.println(state); // TIMED_WAITING（阻塞状态）
        }

        state = t.getState();
        System.out.println(state);// TERMINATED

    }
}
