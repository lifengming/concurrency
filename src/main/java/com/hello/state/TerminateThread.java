package com.hello.state;

/**
 * 模拟：终止线程
 * 1、正常结束
 * 2、外界干预
 * 不要使用stop、destroy方法
 */
public class TerminateThread implements Runnable{

    // 定义一个标志位
    private boolean flag = true;

    private String name;

    public TerminateThread(String name) {
        this.name = name;
    }

    @Override
    public void run() { // 运行状态
        int i=0;
        while (flag){
            System.out.println(name +"--->"+i++);
        }
    }

    public void terminate() {
        this.flag = false;
    }

    public static void main(String[] args) {
        TerminateThread tt = new TerminateThread("CC");
        new Thread(tt).start();// 新生状态->就绪状态

        for (int i=0; i<99; i++) {
            if (i==88){
                tt.terminate(); // 结束状态
                System.out.println("线程终止");
            }
            System.out.println("main--->"+i);
        }
    }
}
