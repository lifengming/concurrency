package com.hello;

/**
 * 优先值常量:
 * MIN_PRIORITY = 1;
 * NORM_PRIORITY=5; 默认
 * MAX_PRIORITY=10;
 *
 * 优先级高 代表被调用的概率高，不代表绝对的先后顺序
 */
public class PriorityTest {

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getPriority());
        MyPriority myPriority = new MyPriority();
        Thread t1 = new Thread(myPriority, "aa");
        Thread t2 = new Thread(myPriority, "bb");
        Thread t3 = new Thread(myPriority,"cc");
        Thread t4 = new Thread(myPriority, "dd");
        Thread t5 = new Thread(myPriority, "ee");
        Thread t6 = new Thread(myPriority,"ff");

        // 设置优先级在启动前
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.setPriority(Thread.MAX_PRIORITY);
        t3.setPriority(Thread.MAX_PRIORITY);
        t4.setPriority(Thread.MIN_PRIORITY);
        t5.setPriority(Thread.MIN_PRIORITY);
        t6.setPriority(Thread.MIN_PRIORITY);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();


    }
    // 结果之一
    // 5
    //cc---->10
    //ff---->1
    //ee---->1
    //aa---->10
    //dd---->1
    //bb---->10
}

class MyPriority implements Runnable {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"---->"+Thread.currentThread().getPriority());
    }
}