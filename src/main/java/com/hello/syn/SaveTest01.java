package com.hello.syn;

/**
 * 线程安全：买票
 * 同步方法
 */
public class SaveTest01 {

    public static void main(String[] args) {
        // 一份资源
        SaveWeb12306 w = new SaveWeb12306();
        // 多个代理
        new Thread(w, "码畜").start();
        new Thread(w, "码农").start();
        new Thread(w, "蚂蟥").start();
    }
}

class SaveWeb12306 implements Runnable {

    private int ticketNums = 10;

    private boolean flag = true;

    @Override
    public void run() {
        while (flag) {
           test();
        }
    }

    /**
     * double check
     */
    public void test() {
        if (ticketNums <= 0) { // 考虑没有票情况
            flag = false;
            return;
        }

        synchronized(this) {
            if (ticketNums <= 0) { // 考虑最后一张票情况
                flag = false;
                return;
            }
            // 模拟网络延时,出现并发数据问题
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+ "--->" +ticketNums--);
        }

    }
}