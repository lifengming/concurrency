package com.hello.syn;

/**
 * 线程不安全：买票
 */
public class UnsaveTest01 {

    public static void main(String[] args) {
        // 一份资源
        UnsaveWeb12306 w = new UnsaveWeb12306();
        // 多个代理
        new Thread(w, "码畜").start();
        new Thread(w, "码农").start();
        new Thread(w, "蚂蟥").start();
    }
}


class UnsaveWeb12306 implements Runnable {

    private int ticketNums = 10;

    private boolean flag = true;

    @Override
    public void run() {
        while (flag) {
           test();
        }
    }

    public void test() {
        if (ticketNums < 0) {
            flag = false;
        }
        // 模拟网络延时,出现并发数据问题
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+ "--->" +ticketNums--);
    }
}