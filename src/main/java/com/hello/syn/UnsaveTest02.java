package com.hello.syn;

/**
 * 线程不安全：取钱
 */
public class UnsaveTest02 {

    public static void main(String[] args) {
        Account account = new Account(100, "结婚礼金");
        Drawing you = new Drawing(account, 80, "你");
        Drawing wife = new Drawing(account, 90, "她");

        you.start();
        wife.start();
    }

}


class Drawing extends Thread{
    Account account;// 取钱账户
    int DrawingMoney; // 取的钱数
    int DrawingTotal; // 口袋的钱

    public Drawing(Account account, int drawingMoney, String name) {
        super(name);
        this.account = account;
        this.DrawingMoney = drawingMoney;
    }

    @Override
    public void run() {
        if (account.momey - DrawingMoney < 0) {
            return;
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        account.momey -= DrawingMoney;
        DrawingTotal += DrawingMoney;
        System.out.println(this.getName()+"------->账户余额为："+account.momey);
        System.out.println(this.getName()+"------->口袋的钱为："+DrawingTotal);

    }
}