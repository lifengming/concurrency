package com.hello.syn;

import java.util.concurrent.CopyOnWriteArrayList;

public class SaveTest04 {

    public static void main(String[] args) throws InterruptedException {
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<>();
        for (int i=0; i< 10000; i++){
            new Thread(() -> {
                // 同步块
                list.add(Thread.currentThread().getName());
            }).start();
        }

        Thread.sleep(1000);
        System.out.println(list.size());
    }
}
