package com.hello;

/**
 * 创建线程方式一
 * 1、创建：继续Thread，重写run方法
 * 2、启动：创建子类对象，调用start方法
 */
public class StartThread extends Thread {

    /**
     * 线程入口点
     */
    @Override
    public void run() {
        for(int i=0; i<5; i++){
            System.out.println("一边听歌");
        }
    }

    public static void main(String[] args) {
        StartThread st = new StartThread();
        st.start(); // 开启一个线程，不保证立即运行，由cpu调用
//      st.run(); // 普通方法调用

        for(int i=0; i<5; i++){
            System.out.println("一边coding");
        }
    }
}
