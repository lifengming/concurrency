package com.hello.join;

/**
 * join 插队线程，当前所在线程体被阻塞
 */
public class JoinDemo02 {
    public static void main(String[] args) {
        System.out.println("这是一个爸爸和儿子买烟的故事");
        new Thread(new Father()).start();
    }
}

class Father implements Runnable{

    @Override
    public void run() {
        System.out.println("想抽烟，发现没了");
        System.out.println("让儿子去买中华");
        Thread t = new Thread(new Son());
        t.start();
        try {
            t.join(); // father被阻塞
            System.out.println("老爸接过烟，把零钱给了儿子");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("孩子走丢了");
        }
    }
}

class Son implements Runnable{

    @Override
    public void run() {
        System.out.println("接过老爸的钱出去了。。。。");
        System.out.println("发现路边有个游戏厅，进去玩了10s");
        for (int i=0; i<10; i++) {
            System.out.println(i+"秒过去了。。。。");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("赶紧买烟去。。。。");
        System.out.println("手拿一包中华回家了。。。。");
    }
}