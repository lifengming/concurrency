package com.hello;

/**
 * 多线程下载图片
 * 多张图片并发下载
 */
public class DownloadThread extends Thread{

    private String url;
    private String name;

    public DownloadThread(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        Download d = new Download();
        d.download(url, name);
        System.out.println(name);
    }

    public static void main(String[] args) {
        DownloadThread dt = new DownloadThread("http://static2.iocoder.cn/images/Learn/08.png", "jc.jpg");
        DownloadThread dt2 = new DownloadThread("http://static2.iocoder.cn/images/Learn/07.png", "yw.jpg");
        DownloadThread dt3 = new DownloadThread("http://static2.iocoder.cn/images/Learn/06.png", "gj.jpg");

        dt.start();
        dt2.start();
        dt3.start();
    }
}
