package com.hello.lambda;

/**
 * lambda表达式推导
 */
public class LambdaThread {

    // 1、静态内部类【父类不使用则不编译】
    static class Test implements Runnable {
        public void run() {
            for(int i=0; i<5; i++){
                System.out.println("一边听歌");
            }
        }
    }


    public static void main(String[] args) {
        // 创建代理类对象
        new Thread(new Test()).start();

        // 2、局部内部类
        class Test2 implements Runnable {
            public void run() {
                for(int i=0; i<5; i++){
                    System.out.println("一边听歌");
                }
            }
        }
        new Thread(new Test2()).start();

        // 3、匿名内部类 省略类名，必须借助接口或父类+实现类体
        new Thread(new Runnable() {
            public void run() {
                for(int i=0; i<5; i++){
                    System.out.println("一边听歌");
                }
            }
        }).start();

        // 4、jdk8简化 lambda,只需关注线程体
        new Thread(() -> {
                for(int i=0; i<5; i++){
                    System.out.println("一边听歌");
                }
            }
        ).start();
    }
}
