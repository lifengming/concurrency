package com.hello.lambda;

public class LambdaTest3 {


    public static void main(String[] args) {
        IInterest interest = (a, b)-> {
            System.out.println("I love Lamdba2---->"+(a+b));
            return a+b;
        };
        interest.lamdba(100, 200);
    }
}

interface IInterest{
    int lamdba(int a, int b);
}

class interest implements IInterest{

    @Override
    public int lamdba(int a, int b) {
        System.out.println("I love Lamdba---->"+(a+b));
        return a+b;
    }
}

