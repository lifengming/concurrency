package com.hello.lambda;

public class LambdaTest4 {

    public static void main(String[] args) {
        new Thread(() -> {
            for(int i=0; i<20; i++) {
                System.out.println("一边学习lamdba");
            }
        }).start();

        new Thread(() -> {
            for(int i=0; i<20; i++) {
                System.out.println("一边崩溃ing");
            }
        }).start();
    }

}

