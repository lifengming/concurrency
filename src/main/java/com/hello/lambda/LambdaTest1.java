package com.hello.lambda;

public class LambdaTest1 {

    // 1、静态内部类
    static class Like2 implements ILike{
        @Override
        public void lamdba() {
            System.out.println("I Like Lamdba2");
        }
    }

    public static void main(String[] args) {
        ILike like = new Like();
        like.lamdba();

        ILike like2 = new Like2();
        like2.lamdba();

        // 2、局部内部类
         class Like3 implements ILike{
            @Override
            public void lamdba() {
                System.out.println("I Like Lamdba3");
            }
        }

        ILike like3 = new Like3();
        like3.lamdba();


        // 3、匿名内部类 借助接口或父类
        ILike like4 = new ILike() {
            public void lamdba() {
                System.out.println("I Like Lamdba4");
            }
        };
        like4.lamdba();

        // 4、jdk8 lambda 只需关注线程体
        ILike like5 = () -> {
                System.out.println("I Like Lamdba5");
            };
        like5.lamdba();

    }
}

interface ILike{
    void lamdba();
}

class Like implements ILike{

    @Override
    public void lamdba() {
        System.out.println("I Like Lamdba");
    }
}

